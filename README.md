# OSX version
Mojave 10.14.5

# Hardware list
- Gigabyte z390 ITX
- i5-9600k
- 32 GB DDR4 3000 CL15
- Integrated graphic card (UHD 630)
- Samsung 970 Pro
- Sharkoon QB one

# Performance
![Disk](diskspeed.png)

# More hardware info of this MB
- [dmidecode](dmidecode.txt)
- [lspci](lspci.txt)
- [alsa-info](alsa-info.txt)

# Remaining issues
- No Wifi
- No Bluetooth
- Sometimes HDMI has no signal. It happens randomly either after wake up or
  when the house voltage is unstable. Trying to check whether disabling `RC6
  (Render Standby)` in BIOS would help.
- ~~Sometimes the audio volume is unstable (the volume oscillates), when a
  headphone is plugged into front panel. (To be verified: this usually happens
  after hibernation?)~~
  ~~Setting `AFGLowPowerState` does not solve this issue.~~
  ~~Now testing whether turning off ResetHDA would solve this problem.~~
  Setting audio injection to 11 seems to fix this (or maybe because I disable hibernation)

# BIOS Setup
Whatever, at least for

- Memory of graphic card
- Above 4G decoding
- XMP
- VT

one can use any setting. But "cec 2019 ready" is not supported!

# Note for installing
## Creating an boot USB
There is a downloader for the full offline version of Majove, even for the case
when Apple Store does not work properly. After the download of the system and
erasing a USB disk, the installation can be performed by
```
Install macOS Mojave.app/Contents/Resources/createinstallmedia --volume /Volumes/myvolume
```

## Booting with USB
With the EFI provided in this repo, it is only possible to boot from a USB2
port (the Sharkoon case has fortunately a pair of front USB ports, no adapter
is needed)

# EFI
I tried different setups, both from scratch or existing one maded by other
people. Some will crash during the installation, some could use USB3 to boot
the installer, the others will meet a "ban" symbol after initializing.  This
provided EFI is based on [an Aorus z390
master](https://github.com/cmer/gigabyte-z390-aorus-master-hackintosh) setup.
As described in that repo, one has to generate device id etc. using a recent
[Clover Configurator](Clover Configurator.zip). It will be able to boot from a
USB2 port.

# USB map
The settings can be tuned using [USBMap](https://github.com/corpnewt/USBMap). A
recent [snapshot](USBMap.zip) is saved, for the case if the official repo would
be one day inaccessible.

In my current configuration, the USB 3.2 ports (both Type-A and C) are
disabled, as I do not have any device which will require these connections.
The ports are shown in the following diagram (Image was made by
[markosmk](https://github.com/markosmk/Pynty-Hackintosh-Gigabyte-Z390-I-Aorus-Pro-Wifi)).
![USB ports on the `Aorus z390 ITX`](ports.jpg)

For the first time setup, you might need USBInjectAll, a [snapshot](RehabMan-USBInjectAll-2018-1108.zip) is also backuped.

My [USB.plist](USB.plist) is available with the following settings:

![USBMap](USBMap.png)

## Notes
-  HS13 and HS14 are disabled, as there might be no solution yet to make them work
-  HS11 and HS12 are the USB 2.0 ports on the front panel

# Other notes
- TRIM for the 970 PRO M.2 NVMe is automatically on.
- TRIM for another 860 EVO M.2 SATA is not enabled by default, which has to be
  switched on by `trimforce`.
- Since I have enough RAM, I am specifying `vm_compressor=2` in boot flag to
  allow RAM compression and disable SWAP.
- For desktops with moderate RAM, it would be better to set "sleep to RAM",
  rather than writing RAM to SSD. It can be achieved with

```
  sudo pmset -a hibernatemode 0
  # For the complete disabling of the hibernation image:
  sudo pmset -a standby 0
  sudo pmset -a autopoweroff 0
  # It seems that sometimes system cannot wake up (at least no HDMI output), when hibernation is off.
```

# Majove notes
- As the other Mac users noticed, the `noauto` in `fstab` in majove ONLY works with `LABEL=...`
- Maybe I want noatime, as it was always enabled in my linux boxes
